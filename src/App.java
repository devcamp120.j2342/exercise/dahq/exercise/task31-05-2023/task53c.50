import javax.sound.midi.MidiDevice;

import model.MovableCircle;
import model.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint movablePoint = new MovablePoint(6, 7);

        movablePoint.moveLeft();
        movablePoint.moveDown();
        //
        System.out.println(movablePoint.toString());

        MovableCircle movableCircle = new MovableCircle(9, movablePoint);
        System.out.println(movableCircle.toString());
    }
}
