package model;

public class MovableCircle implements Movable {
    private int radius;
    private MovablePoint movablePoint;

    @Override
    public void moveUp() {
        movablePoint.moveUp();
    }

    @Override
    public void moveDown() {
        movablePoint.moveDown();
    }

    @Override
    public void moveLeft() {
        movablePoint.moveRight();
    }

    @Override
    public void moveRight() {
        movablePoint.moveRight();
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public MovablePoint getMovablePoint() {
        return movablePoint;
    }

    public void setMovablePoint(MovablePoint movablePoint) {
        this.movablePoint = movablePoint;
    }

    public MovableCircle(int radius, MovablePoint movablePoint) {
        this.radius = radius;
        this.movablePoint = movablePoint;
    }

    @Override
    public String toString() {
        return "MovableCircle [radius=" + radius + ", movablePoint=" + movablePoint.toString() + "]";
    }

}
