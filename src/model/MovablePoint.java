package model;

public class MovablePoint implements Movable {
    private int x;
    private int y;

    @Override
    public void moveUp() {
        this.y++;
        System.out.println(this.y);
    }

    @Override
    public void moveDown() {
        this.y = this.y - 1;
        System.out.println(this.y);
    }

    @Override
    public void moveLeft() {
        this.x--;
        System.out.println(this.x);
    }

    @Override
    public void moveRight() {
        this.x++;
        System.out.println(this.x);
    }

    @Override
    public String toString() {
        return "MovablePoint(x,y) " + "speed(" + this.x + "," + this.y + ")";
    }

    public MovablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
